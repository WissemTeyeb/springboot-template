package com.example.demo.controllers;


import com.example.demo.security.UserAuthentification;
import com.example.demo.security.userApp;
import com.example.demo.service.ToDoService;
import com.example.demo.model.ToDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="/url/api/v1")
public class BasePathController extends  UserAuthentification {
    @Autowired
    private ToDoService service;
    @GetMapping(value= "/{name}")
    public String getNameController(@PathVariable String name){

        return "hello from url/api/v1 "+name  ;

    }

    @GetMapping(value= "/users")
    public List<userApp> getUsersController(){

        List<userApp> result= service.getUsers();
        System.out.println(result);
        return result;

    }
    @GetMapping(value= "/data")
    public List<ToDO> getDataByUserController(){
        List<ToDO> result= service.getDataById(super.getUserId());
        return result;

    }

    @PostMapping(value= "/data/add")
    public ResponseEntity<ToDO> saveDataController(@RequestBody ToDO o){
        o.setId(super.getUserId());
        ToDO result =  service.saveObject(o);
      return new ResponseEntity<>(result, HttpStatus.CREATED);
    }
    @DeleteMapping(value= "/data/remove/{id}")
    public ResponseEntity<ToDO> removeDataController(@PathVariable  String id){
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
 }
}
