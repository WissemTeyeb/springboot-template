package com.example.demo.controllers;

import com.example.demo.model.ToDO;
import com.example.demo.service.ToDoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment =SpringBootTest.WebEnvironment.RANDOM_PORT)//give random port for testing
@AutoConfigureMockMvc //we simulate http request ,also help us to inject mockMvc
public class BasePathControllerTest {

    @Autowired
    MockMvc mockMvc;
    @MockBean
    ToDoService toDoService;
    @Test
    public void testGetAllTodDOsController()throws  Exception{
     /*   ToDO data1=new ToDO("1", "wiss","teyeb");
        ToDO data2=new ToDO("1", "siwar","teyeb");

        List<ToDO> mockdata= Arrays.asList(data1, data2);


        given(toDoService.getObject()).willReturn(mockdata);
        mockMvc.perform(get("/url/api/v1/data"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",hasSize(2)))
                .andExpect(jsonPath("$[0].firstName",equalTo("wiss")));*/
    }
    @Test
    public void testInsertTodDOsController()throws  Exception{
        ToDO data1=new ToDO("1","1","wiss","teyeb");

        ObjectMapper mapper = new ObjectMapper() ;
        given(toDoService.saveObject(Mockito.any(ToDO.class))).willReturn(data1);
        mockMvc.perform(post("/url/api/v1/data/add")
                .contentType(MediaType.asMediaType(MediaType.APPLICATION_JSON))
                .content(mapper.writeValueAsString(data1))
        )
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.firstName",equalTo("wiss")));
    }
}
